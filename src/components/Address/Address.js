import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { 
    faPhone, faEnvelope,
 } from "@fortawesome/free-solid-svg-icons";
import { faFacebook, faTwitter, faSkype, faInstagram } from "@fortawesome/free-brands-svg-icons";
import './address.scss'

function Address() {
    return(
        <address className="app-section__address">
            <dl>
                <dt>
                    <FontAwesomeIcon icon={faPhone} size="lg" />
                </dt>
                <dd>
                    <a href="tel:595 95 13 38">
                        <strong>595 95 13 38</strong>
                    </a>
                </dd>
            </dl>
            <dl>
                <dt>
                    <FontAwesomeIcon icon={faEnvelope} size="lg" />
                </dt>
                <dd>
                    <a href="mailto:Dach.kapanadze@gmail.com">
                        <strong>Dach.kapanadze@gmail.com</strong>
                    </a>
                </dd>
            </dl>
            <dl>
                <dt>
                    <FontAwesomeIcon icon={faTwitter} size="lg" />
                </dt>
                <dd>
                    <strong>Twitter</strong> <br />
                    <a href="https://twitter.com/DachiKapan37233">https://twitter.com/DachiKapan37233</a>
                </dd>
            </dl>
            <dl>
                <dt>
                    <FontAwesomeIcon icon={faFacebook} size="lg" />
                </dt>
                <dd>
                    <strong>Facebook</strong> <br />
                    <a href="https://www.facebook.com/dachi.kapanadze.7/">https://www.facebook.com/dachi.kapanadze.7/</a>
                </dd>
            </dl>
            <dl>
                <dt>
                    <FontAwesomeIcon icon={faInstagram} size="lg" />
                </dt>
                <dd>
                    <strong>Instragram</strong> <br />
                    <a href="https://www.instagram.com/dachii.kapaanadze/">https://www.instagram.com/dachii.kapaanadze/</a>
                </dd>
            </dl>
        </address>
    )
}

export default Address;