import { createServer, Model } from "miragejs";

export default function() {
    createServer({
        models: {
            education: Model,
            skills: Model,
        },

        seeds(server) {
            server.create("education", { date: '2006-2018', title: "24th Georgian Gymnasium" });
            server.create("education", { date: '2019-2023', title: "Tbilisi State Medical University"});
            server.create("education", { date: '2023', title: "Epam", description: "Front-End Development" });
        },
    
        routes() {
            this.get("api/educations", (schema) => {
                return schema.educations.all()
            }, { timing: 3000 })
    
            this.get("api/skills", (schema) => {
                return schema.skills.all()
            }, { timing: 3000 })

            this.post("/api/skills", (schema, request) => {
                const skillData = JSON.parse(request.requestBody);
                const skill = schema.skills.create(skillData);

                return skill;
            })
        },
    })
}