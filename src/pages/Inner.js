import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleUp } from "@fortawesome/free-solid-svg-icons";
import Panel from "../components/Panel/Panel";
import Box from "../components/Box/Box";
import TimeLine from "../components/Timeline/TimeLine";
import Expertise from "../components/Expertise/Expertise";
import Feedback from "../components/Feedback/Feedback";
import feedbackUser from '../assets/feedback-user.jpg';
import Portfolio from "../components/Portfolio/Portfolio";
import Address from "../components/Address/Address";
import Skills from '../components/Skills/Skills';

function Inner() {
    return(
        <>
            <Panel />
            <main id="main-inner">
                <Box
                    id="about-me"
                    title="About me"
                    content="My name is Dachi Kapanadze. I was born and raised in Rustavi, Georgia. I have finished 24th Georgian Gymnasium in 2018. In 2023 I became a Bachelor at Healthcare, in Tbilisi State Medical University. I have been working at Georgian Harm Reduction Network for almost three years now. I am a mobile ambulatories coordinator. I started studying Front-end development at EPAM nearly 6 months ago. I am a very sociable person, really love what I'm doing and hope I will become a successful Front-end developer one day."
                />
                <Box 
                id="education" 
                title="Education">
                    <TimeLine />
                </Box>
                <Box title="Experience" id="experience">
                    <Expertise data={[ { 
                    date: '2020-2023', 
                    info: 
                        {
                            company: 'Georgian Harm Reduction Network', 
                            job: 'Mobile Ambulatories Coordinator', 
                            description: 'This is my first full-time job, I have been working here for 3 years, and It tought me a lot. I learned the real power of teamwork, I learned to be more responsible, know the true value of doing something you love.' 
                        } 
                    }, 
                    
                ]} />
                </Box>
                <Skills />
                <Box title="Portfolio" id="portfolio">
                    <Portfolio />
                </Box>
                <Box title="Contacts" id="contacts">
                    <Address />
                </Box>
                <Box title="Feedbacks" id="feedbacks">
                    <Feedback data={[ {
                        feedback: ' Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
                        reporter: { 
                            photoUrl: feedbackUser, 
                            name: 'John Doe', 
                            citeUrl: 'https://www.citeexample.com' 
                        } 
                    }, 
                    {
                        feedback: ' Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
                        reporter: { 
                            photoUrl: feedbackUser, 
                            name: 'John Doe', 
                            citeUrl: 'https://www.citeexample.com' 
                        }
                    } ]} />
                </Box>
                <a id="scroll-up-button" href='#about-me'>
                    <FontAwesomeIcon icon={faAngleUp} size="lg" />
                </a>
            </main>
        </>
    )
}

export default Inner;