import PhotoBox from "../components/PhotoBox/PhotoBox";
import userAvi from '../assets/avatar.jpg';
import Button from '../components/Button/Button';

function Home() {
    return(
        <main id="main-home">
            <PhotoBox
                imgSrc={userAvi}
                fullName="Dachi Kapanadze"
                position="Future Front-End Developer"
                description="Welcome to my CV, Pleasure to meet you!  "
            >
                <Button text="Know more" to="/inner" />
            </PhotoBox>
        </main>
    )
}

export default Home;